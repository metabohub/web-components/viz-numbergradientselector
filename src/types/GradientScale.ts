export interface GradientScale {
  [key:string]: {
    [key:string]: number | string | boolean
  };
}