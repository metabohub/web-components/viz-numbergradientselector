import * as d3 from 'd3';

export function createNumberScale(domainScale: Array<number>, rangeScale: Array<number>) {
  const minDomain = domainScale[0];
  const maxDomain = domainScale[1];

  const minRange = rangeScale[0];
  const maxRange = rangeScale[1];

  const numberScale = d3.scaleLinear()
    .domain([minDomain, maxDomain])
    .range([minRange, maxRange]);

  return numberScale;
}

export function getNumberScale(gradient: { [key: string]: { [key: string]: number } }) {
  let numberDomain: Array<number> = [];
  let numberRange: Array<number> = [];

  numberDomain.push(gradient['min'].value);
  numberRange.push(gradient['min'].number);

  const stepList: Array<number[]> = [];

  Object.keys(gradient).forEach((step) => {
    if (step !== 'min' && step !== 'max') {
      const stroke = gradient[step];
      const value = stroke.value;
      const number = stroke.number;
      const stepData = [value, number];
      stepList.push(stepData);
    }
  });

  if (stepList.length > 1) {
    stepList.sort((a, b) => {
      return a[0] - b[0];
    });
  }

  if (stepList.length > 0) {
    for (let i = 0; i < stepList.length; i++) {
      numberDomain.push(stepList[i][0]);
      numberRange.push(stepList[i][1]);
    }
  }

  numberDomain.push(gradient['max'].value);
  numberRange.push(gradient['max'].number);

  const numberScale = d3.scaleLinear()
    .domain(numberDomain)
    .range(numberRange);

  return numberScale;
}

export function computeGradientRepresentation(gradient: { [key: string]: { [key: string]: number } }, linearScale: d3.ScaleLinear<number, number, never>) {
  let svgGradientDisplay: string;

  const heightSection = 52;
  const nbStep = Object.keys(gradient).length;

  let yStart = 52;
  let xStart = linearScale(gradient['min'].value) + 52;

  let yEnd = heightSection * nbStep + 52;
  let xEnd = linearScale(gradient['max'].value) + 52;

  if (nbStep === 2) {
    svgGradientDisplay = 'M 52 ' + yStart + ' L 52 ' + yEnd + ' L ' + xEnd + ' ' + yEnd + ' L ' + xStart + ' ' + yStart + ' L 52 ' + yStart;

    return svgGradientDisplay;
  }

  if (nbStep > 2) {
    svgGradientDisplay = 'M 52 ' + yStart + ' L 52 ' + yEnd + ' L ' + xEnd + ' ' + yEnd;

    const intermediateValues = sortGradientStep(gradient);

    for (let i = 0; i < nbStep - 2; i++) {
      let val = intermediateValues[i].value;
      let valToY = val - gradient['min'].value;
      let delta = gradient['max'].value - gradient['min'].value;

      let xPos = linearScale(val) + 52;
      let yPos = (((heightSection * nbStep) * valToY) / delta);
      yPos += 52;

      svgGradientDisplay += ' L ' + xPos + ' ' + yPos;
    }

    svgGradientDisplay += ' L ' + xStart + ' ' + yStart + ' L 52 ' + yStart;

    return svgGradientDisplay;
  }
}

function sortGradientStep(gradient: { [key: string]: { [key: string]: number } }) {
  const steps = Object.keys(gradient);
  const values = [] as Array<{[key: string]: number}>;

  steps.forEach((step) => {
    if (step !== 'min' && step !== 'max') {
      values.push({value: gradient[step].value as number, number: gradient[step].number as number});
    }
  });

  values.sort((a, b) => {
    return b.value - a.value;
  });

  return values;
}