/**
 * @vitest-environment jsdom
 */

import { describe, test, expect } from 'vitest';

import { createVuetify } from "vuetify";

import { mount } from '@vue/test-utils';

import NumberGradientSelector from '../NumberGradientSelector.vue';

function mappingWindowWrapper() {
	const vuetify = createVuetify()
	return mount(NumberGradientSelector, {
		global: {
			plugins: [vuetify]
		}
	});
}

describe('Test for MappingWindow component', () => {
  test('Input file area', () => {
    const wrapper = mappingWindowWrapper();

    expect(wrapper.exists()).toBeTruthy();
  });
});